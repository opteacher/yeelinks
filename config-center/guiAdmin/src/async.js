import axios from "axios"

function getErrorMsg(res) {
    if (typeof res === "string") {
        return res
    } else if (res.message) {
        return res.message
    } else if (res.data.message) {
        return res.data.message
    } else {
        return null
    }
}

export default {
    async listProjects() {
        try {
            return await axios.post("http://127.0.0.1:8000/config-center/backend/configs/list/projects")
        } catch(e) {
            return getErrorMsg(e)
        }
    },
    async types() {
        try {
            return await axios.post("http://127.0.0.1:8000/config-center/backend/configs/types")
        } catch (e) {
            return getErrorMsg(e)
        }
    },
    async addConfig(config) {
        try {
            return await axios.post("http://127.0.0.1:8000/config-center/backend/configs/set", config)
        } catch (e) {
            return getErrorMsg(e)
        }
    },
    async qryConfigs(project) {
        try {
            return await axios.post("http://127.0.0.1:8000/config-center/backend/configs/all", {project})
        } catch(e) {
            return getErrorMsg(e)
        }
    },
    async updConfigByID(config) {
        try {
            return await axios.post("http://127.0.0.1:8000/config-center/backend/configs/update", config)
        } catch (e) {
            return getErrorMsg(e)
        }
    },
    async delConfigByID(id) {
        try {
            return await axios.post("http://127.0.0.1:8000/config-center/backend/configs/delete", {id})
        } catch (e) {
            return getErrorMsg(e)
        }
    },
    async copyConfigs(srcProj, tgtProj) {
        try {
            return await axios.post("http://127.0.0.1:8000/config-center/backend/configs/copy", {srcProj, tgtProj})
        } catch (e) {
            return getErrorMsg(e)
        }
    }
}