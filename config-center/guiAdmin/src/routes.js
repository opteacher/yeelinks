import newConfig from "./pages/newConfig"
import setConfig from "./pages/setConfig"

export default [{
    path: "/",
    redirect: "/config-center/frontend/new_config"
}, {
    path: "/config-center/frontend/new_config",
    component: newConfig
}, {
    path: "/config-center/frontend/set_config",
    component: setConfig
}]