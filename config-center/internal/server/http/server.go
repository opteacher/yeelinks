package http

import (
	pb "config-center/api"
	"config-center/internal/service"
	"config-center/internal/server/mws"
	svr "config-center/internal/server"

	"github.com/bilibili/kratos/pkg/conf/paladin"
	bm "github.com/bilibili/kratos/pkg/net/http/blademaster"
)

var (
	svc *service.Service
)

// New new a bm server.
func New(s *service.Service) (engine *bm.Engine) {
	var (
		hc struct {
			Server *bm.ServerConfig
		}
	)
	if err := paladin.Get("http.toml").UnmarshalTOML(&hc); err != nil {
		if err != paladin.ErrNotExist {
			panic(err)
		}
	}
	svc = s
	engine = bm.DefaultServer(hc.Server)
	engine.Use(mws.SetupCORS())
	pb.RegisterConfigCenterBMServer(engine, svc)
	svr.RegisterHTTPService(svc, "SERVICE_ADDRS")
	if err := engine.Start(); err != nil {
		panic(err)
	}
	return
}
