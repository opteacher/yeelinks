package grpc

import (
	pb "config-center/api"
	"config-center/internal/service"
	svr "config-center/internal/server"

	"github.com/bilibili/kratos/pkg/conf/paladin"
	"github.com/bilibili/kratos/pkg/net/rpc/warden"
)

// New new a grpc server.
func New(svc *service.Service) *warden.Server {
	var rc struct {
		Server *warden.ServerConfig
	}
	if err := paladin.Get("grpc.toml").UnmarshalTOML(&rc); err != nil {
		if err != paladin.ErrNotExist {
			panic(err)
		}
	}
	ws := warden.NewServer(rc.Server)
	pb.RegisterConfigCenterServer(ws.Server(), svc)
	svr.RegisterGRPCService(svc.AppID(), "SERVICE_ADDRS")
	ws, err := ws.Start()
	if err != nil {
		panic(err)
	}
	return ws
}
