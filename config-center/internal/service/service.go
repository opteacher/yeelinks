package service

import (
	"fmt"
	"strings"
	"encoding/json"
	"context"
	"reflect"

	pb "config-center/api"
	"config-center/internal/dao"
	"config-center/internal/model"
	"config-center/internal/utils"

	"github.com/bilibili/kratos/pkg/conf/paladin"
)

// Service service.
type Service struct {
	ac  *paladin.Map
	dao *dao.Dao
}

// New new a service and return.
func New() (s *Service) {
	var ac = new(paladin.TOML)
	if err := paladin.Watch("application.toml", ac); err != nil {
		panic(err)
	}
	s = &Service{
		ac:  ac,
		dao: dao.New(),
	}
	if tx, err := s.dao.BeginTx(context.Background()); err != nil {
		panic(err)
	} else if err := s.dao.CreateTx(tx, model.CFG_TABLE, reflect.TypeOf((*pb.Config)(nil)).Elem()); err != nil {
		panic(err)
	} else if err := s.dao.CommitTx(tx); err != nil {
		panic(err)
	}
	return s
}

func (s *Service) AppID() string {
	appID, _ := s.ac.Get("appID").String()
	return appID
}

func (s *Service) SwaggerFile() string {
	swagger, _ := s.ac.Get("swaggerFile").String()
	return swagger
}

func genCond(proj string, topic string, section string, key string) (string, []interface{}) {
	aCondStr := make([]string, 0)
	condArg := make([]interface{}, 0)
	if len(proj) != 0 {
		aCondStr = append(aCondStr, "`project`=?")
		condArg = append(condArg, proj)
	}
	if len(topic) != 0 {
		aCondStr = append(aCondStr, "`topic`=?")
		condArg = append(condArg, topic)
	}
	if len(section) != 0 {
		aCondStr = append(aCondStr, "`section`=?")
		condArg = append(condArg, section)
	}
	if len(key) != 0 {
		aCondStr = append(aCondStr, "`key`=?")
		condArg = append(condArg, key)
	}
	return strings.Join(aCondStr, " AND "), condArg
}

func genCondByCfg(cfg *pb.Config) (string, []interface{}) {
	return genCond(cfg.Project, cfg.Topic, cfg.Section, cfg.Key)
}

func genCondByIden(iden *pb.IdenCfg) (string, []interface{}) {
	return genCond(iden.Project, iden.Topic, iden.Section, iden.Key)
}

func (s *Service) Set(ctx context.Context, req *pb.Config) (*pb.Config, error) {
	if tx, err := s.dao.BeginTx(ctx); err != nil {
		return nil, fmt.Errorf("开启事务失败：%v", err)
	} else if cstr, carg := genCondByCfg(req); false {
		return nil, nil
	} else if bytes, err := json.Marshal(req); err != nil {
		return nil, fmt.Errorf("转化JSON失败：%v", err)
	} else if mp, err := utils.UnmarshalJSON(bytes, reflect.TypeOf((*map[string]interface{})(nil)).Elem()); err != nil {
		return nil, fmt.Errorf("转化Map失败：%v", err)
	} else if id, err := s.dao.SaveTx(tx, model.CFG_TABLE, cstr, carg, *(mp.(*map[string]interface{})), true); err != nil {
		return nil, fmt.Errorf("设置配置失败：%v", err)
	} else {
		req.Id = id
		return req, nil
	}
}

func (s *Service) Get(ctx context.Context, req *pb.IdenCfg) (*pb.Config, error) {
	if cstr, carg := genCondByIden(req); false {
		return nil, nil
	} else if tx, err := s.dao.BeginTx(ctx); err != nil {
		return nil, fmt.Errorf("开启事务失败：%v", err)
	} else if mps, err := s.dao.QueryTx(tx, model.CFG_TABLE, cstr, carg); err != nil {
		return nil, fmt.Errorf("查询配置失败：%v", err)
	} else if len(mps) == 0 {
		return nil, nil
	} else if err := s.dao.CommitTx(tx); err != nil {
		return nil, fmt.Errorf("提交事务失败：%v", err)
	} else if bytes, err := json.Marshal(mps[0]); err != nil {
		return nil, fmt.Errorf("转化JSON失败：%v")
	} else if res, err := utils.UnmarshalJSON(bytes, reflect.TypeOf((*pb.Config)(nil)).Elem()); err != nil {
		return nil, fmt.Errorf("转化Config失败：%v", err)
	} else {
		return res.(*pb.Config), nil
	}
}

func (s *Service) All(ctx context.Context, req *pb.IdenCfg) (*pb.Configs, error) {
	if cstr, carg := genCondByIden(req); false {
		return nil, nil
	} else if tx, err := s.dao.BeginTx(ctx); err != nil {
		return nil, fmt.Errorf("开启事务失败：%v", err)
	} else if mps, err := s.dao.QueryTx(tx, model.CFG_TABLE, cstr, carg); err != nil {
		return nil, fmt.Errorf("查询配置失败：%v", err)
	} else if err := s.dao.CommitTx(tx); err != nil {
		return nil, fmt.Errorf("提交事务失败：%v", err)
	} else {
		resp := new(pb.Configs)
		for _, mp := range mps {
			if bytes, err := json.Marshal(mp); err != nil {
				return nil, fmt.Errorf("转化JSON失败：%v")
			} else if cfg, err := utils.UnmarshalJSON(bytes, reflect.TypeOf((*pb.Config)(nil)).Elem()); err != nil {
				return nil, fmt.Errorf("转化Config失败：%v", err)
			} else {
				resp.Configs = append(resp.Configs, cfg.(*pb.Config))
			}
		}
		return resp, nil
	}
}

func (s *Service) UpdateByID(ctx context.Context, req *pb.Config) (*pb.Config, error) {
	if bytes, err := json.Marshal(req); err != nil {
		return nil, fmt.Errorf("转化JSON失败：%v", err)
	} else if mp, err := utils.UnmarshalJSON(bytes, reflect.TypeOf((*map[string]interface{})(nil)).Elem()); err != nil {
		return nil, fmt.Errorf("转化MAP失败：%v", err)
	} else if tx, err := s.dao.BeginTx(ctx); err != nil {
		return nil, fmt.Errorf("开启事务失败：%v", err)
	} else if _, err := s.dao.UpdateTxByID(tx, model.CFG_TABLE, *(mp.(*map[string]interface{}))); err != nil {
		return nil, fmt.Errorf("更新配置失败：%v", err)
	} else if err := s.dao.CommitTx(tx); err != nil {
		return nil, fmt.Errorf("提交事务失败：%v", err)
	} else {
		return req, nil
	}
}

func (s *Service) DeleteByID(ctx context.Context, req *pb.Iden) (*pb.Config, error) {
	if tx, err := s.dao.BeginTx(ctx); err != nil {
		return nil, fmt.Errorf("开启事务失败：%v", err)
	} else if mp, err := s.dao.QueryTxByID(tx, model.CFG_TABLE, req.Id); err != nil {
		return nil, fmt.Errorf("查询配置失败：%v", err)
	} else if _, err := s.dao.DeleteTxByID(tx, model.CFG_TABLE, req.Id); err != nil {
		return nil, fmt.Errorf("删除配置失败：%v", err)
	} else if err := s.dao.CommitTx(tx); err != nil {
		return nil, fmt.Errorf("提交事务失败：%v", err)
	} else if delete(mp, "id"); false {
		return nil, nil
	} else if bytes, err := json.Marshal(mp); err != nil {
		return nil, fmt.Errorf("转化JSON失败：%v", err)
	} else if resp, err := utils.UnmarshalJSON(bytes, reflect.TypeOf((*pb.Config)(nil)).Elem()); err != nil {
		return nil, fmt.Errorf("转化Config失败：%v", err)
	} else {
		return resp.(*pb.Config), nil
	}
}

func (s *Service) Copy(ctx context.Context, req *pb.CopyReqs) (*pb.String, error) {
	if tx, err := s.dao.BeginTx(ctx); err != nil {
		return nil, fmt.Errorf("开启事务失败：%v", err)
	} else if mps, err := s.dao.QueryTx(tx, model.CFG_TABLE, "`project`=?", []interface{}{req.SrcProj}); err != nil {
		return nil, fmt.Errorf("查询配置失败：%v", err)
	} else {
		for _, mp := range mps {
			if mp["project"] = req.TgtProj; false {
				return nil, nil
			} else if delete(mp, "id"); false {
				return nil, nil
			} else if _, err := s.dao.InsertTx(tx, model.CFG_TABLE, mp); err != nil {
				return nil, fmt.Errorf("插入新配置失败：%v", err)
			}
		}
		if err := s.dao.CommitTx(tx); err != nil {
			return nil, fmt.Errorf("提交事务失败：%v", err)
		}
		return &pb.String{ Value: req.TgtProj }, nil
	}
}

func (s *Service) ListProjs(ctx context.Context, req *pb.Nil) (*pb.StrArray, error) {
	if tx, err := s.dao.BeginTx(ctx); err != nil {
		return nil, fmt.Errorf("开启事务失败：%v", err)
	} else if res, err := s.dao.QueryTxIdenCol(tx, model.CFG_TABLE, []string{"DISTINCT `project`"}, "", nil); err != nil {
		return nil, fmt.Errorf("查询项目列表失败：%v", err)
	} else if err := s.dao.CommitTx(tx); err != nil {
		return nil, fmt.Errorf("提交事务失败：%v", err)
	} else {
		resp := new(pb.StrArray)
		for _, mp := range res {
			resp.Values = append(resp.Values, mp["project"].(string))
		}
		return resp, nil
	}
}

func (s *Service) Types(context.Context, *pb.Nil) (*pb.TypesResp, error) {
	return &pb.TypesResp{
		Types: pb.CfgType_name,
	}, nil
}

// Ping ping the resource.
func (s *Service) Ping(ctx context.Context) (err error) {
	return s.dao.Ping(ctx)
}

// Close close the resource.
func (s *Service) Close() {
	s.dao.Close()
}
