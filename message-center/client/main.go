package main

import (
	"context"
	"github.com/bilibili/kratos/pkg/net/rpc/warden"
	pb "message-center/api"
	"fmt"
)

func main() {
	// 采用grpc直连的方式向服务器发送publish请求
	cli := warden.NewClient(nil)
	conn, err := cli.Dial(context.Background(), "direct://default/127.0.0.1:9000")
	if err != nil {
		panic(err)
	}
	msgCenCli := pb.NewMessageCenterClient(conn)
	resp, _ := msgCenCli.GetClients(context.Background(), &pb.Nil{})
	if len(resp.Clients) != 0 {
		appID := resp.Clients[0].AppID
		fmt.Printf("Message '1234' will send to %s\n", appID)
		msgCenCli.Publish(context.Background(), &pb.Message{
			AppID: appID,
			Data: []byte("1234"),
		})
	}
}
