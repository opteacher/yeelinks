// Code generated by protoc-gen-bm v0.1, DO NOT EDIT.
// source: api/api.proto

/*
Package api is a generated blademaster stub package.
This code was generated with kratos/tool/protobuf/protoc-gen-bm v0.1.

package 命名使用 {appid}.{version} 的方式, version 形如 v1, v2 ..

It is generated from these files:
	api/api.proto
*/
package api

import (
	"context"

	bm "github.com/bilibili/kratos/pkg/net/http/blademaster"
	"github.com/bilibili/kratos/pkg/net/http/blademaster/binding"
)

// to suppressed 'imported but not used warning'
var _ *bm.Context
var _ context.Context
var _ binding.StructValidator

// MessageCenterBMServer is the server API for MessageCenter service.
type MessageCenterBMServer interface {
}

var MessageCenterSvc MessageCenterBMServer

// RegisterMessageCenterBMServer Register the blademaster route
func RegisterMessageCenterBMServer(e *bm.Engine, server MessageCenterBMServer) {
	MessageCenterSvc = server
}
