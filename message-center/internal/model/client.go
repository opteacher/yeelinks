package model

import (
	"github.com/gorilla/websocket"
	"time"
	"github.com/pquerna/ffjson/ffjson"
	"github.com/bilibili/kratos/pkg/log"
)

// Client is a middleman between the websocket connection and the hub.
type Client struct {
	Hub  *Hub
	Conn *websocket.Conn
	Send chan []byte
}

// writePump 推送消息
func (c *Client) WritePump() {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		c.Conn.Close()
	}()
	for {
		select {
		case message, ok := <-c.Send:
			c.Conn.SetWriteDeadline(time.Now().Add(writeWait))
			if !ok {
				c.Conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}
			w, err := c.Conn.NextWriter(websocket.TextMessage)
			if err != nil {
				return
			}
			w.Write(message)
			if err := w.Close(); err != nil {
				return
			}
		case <-ticker.C:
			c.Conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := c.Conn.WriteMessage(websocket.PingMessage, []byte{}); err != nil {
				return
			}
		}
	}
}

func (c *Client) ReadPump() {
	log.Info("start read message")
	defer func() {
		c.Hub.Unregister <- c
		c.Conn.Close()
		log.Info("websocket close")
	}()
	c.Conn.SetReadLimit(maxMessageSize)
	c.Conn.SetReadDeadline(time.Now().Add(pongWait))
	c.Conn.SetPongHandler(func(string) error {
		c.Conn.SetReadDeadline(time.Now().Add(pongWait))
		return nil
	})
	for {
		_, message, err := c.Conn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway) {
				log.Error("websocket.IsUnexpectedCloseError: (%v)", err)
			}
			log.Error("websocket read message have err: (%v)", err)
			break
		}
		if c.Hub.OnMessage != nil {
			if err := c.Hub.OnMessage(message, c.Hub); err != nil {
				log.Error("websocket read message error: %v", err)
				break
			}
		}
		reply := map[string]string{
			"event_type": "register",
			"data":       "ok",
		}
		b, _ := ffjson.Marshal(reply)
		c.Send <- b
	}
}