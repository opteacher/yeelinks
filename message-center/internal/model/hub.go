package model

// OnMessageFunc 接收到消息触发事件
type OnMessageFunc func(message []byte, hub *Hub) error

// Hub maintains the set of active clients and broadcasts message to the clients.
type Hub struct {
	Clients    map[*Client]bool
	Broadcast  chan []byte
	OnMessage  OnMessageFunc
	Register   chan *Client
	Unregister chan *Client
}

// NewHub 分配一个新的Hub
func NewHub(onEvent OnMessageFunc) *Hub {
	return &Hub{
		Broadcast:  make(chan []byte),      // 向前台传递的数据
		Register:   make(chan *Client),     // 新连接
		Unregister: make(chan *Client),     // 断开的连接
		Clients:    make(map[*Client]bool), // 包含所有的client连接信息
		OnMessage:  onEvent,
	}
}

func (h *Hub) Run() {
	for {
		select {
		case client := <-h.Register:
			h.Clients[client] = true
		case client := <-h.Unregister:
			if _, ok := h.Clients[client]; ok {
				delete(h.Clients, client)
				close(client.Send)
			}
		case message := <-h.Broadcast:
			for client := range h.Clients {
				select {
				case client.Send <- message:
				default:
					close(client.Send)
					delete(h.Clients, client)
				}
			}
		}
	}
}