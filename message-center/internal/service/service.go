package service

import (
	"context"
	pb "message-center/api"
	//"message-center/internal/dao"

	"github.com/bilibili/kratos/pkg/conf/paladin"

	"message-center/internal/server"

	"net/http"
	"github.com/gorilla/websocket"
	"github.com/bilibili/kratos/pkg/log"
	"github.com/pkg/errors"
	"message-center/internal/model"
	"fmt"
)

// Service service.
type Service struct {
	ac  *paladin.Map
	//dao *dao.Dao
	upgrader websocket.Upgrader
	cliMapper map[string]*model.Client
	hub *model.Hub
}

// New new a service and return.
func New() (s *Service) {
	var ac = new(paladin.TOML)
	if err := paladin.Watch("application.toml", ac); err != nil {
		panic(err)
	}
	s = &Service{
		ac:  ac,
		//dao: dao.New(),
		upgrader: websocket.Upgrader{
			ReadBufferSize:  1024,
			WriteBufferSize: 1024,
			CheckOrigin: func(r *http.Request) bool {
				return true
			},
		},
		cliMapper: make(map[string]*model.Client),
		hub: model.NewHub(func(message []byte, hub *model.Hub) error {
			log.Info("Intercepted message: %s", string(message))
			return nil
		}),
	}
	// 开启hub监听
	go s.hub.Run()
	return s
}

func (s *Service) AppID() string {
	appID, _ := s.ac.Get("appID").String()
	return appID
}

// SayHello grpc demo func.
func (s *Service) GetClients(context.Context, *pb.Nil) (*pb.Clients, error) {
	resp := new(pb.Clients)
	for appID, cli := range s.cliMapper {
		resp.Clients = append(resp.Clients, &pb.Client{
			AppID: appID,
			Ip: cli.Conn.RemoteAddr().String(),
		})
	}
	return resp, nil
}

func (s *Service) Publish(ctx context.Context, req *pb.Message) (*pb.Nil, error) {
	cli, ok := s.cliMapper[req.AppID]
	if !ok {
		return nil, fmt.Errorf("Client %s unregistered", req.AppID)
	}
	cli.Send <- req.Data
	return &pb.Nil{}, nil
}

func (s *Service) ServeWs(w http.ResponseWriter, r *http.Request) error {
	conn, err := s.upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Error("websocket: connect err (%v)", err)
		return err
	}

	log.Info("websocket: Servews is start")
	params := r.URL.Query()
	_token := params["token"][0]
	if _token == "undefined" || _token == "null" {
		return errors.New("No user token in the header")
	}
	// TODO: 鉴权获得AppID，最好搬到中间件中去
	user := &struct {
		AppID string
	}{"abcd"}
	client := &model.Client{
		Hub: s.hub,
		Conn: conn,
		Send: make(chan []byte, 256),
	}
	client.Hub.Register <- client
	s.cliMapper[user.AppID] = client
	go client.WritePump()
	client.ReadPump()
	return nil
}

// Ping ping the resource.
func (s *Service) Ping(ctx context.Context) (err error) {
	//return s.dao.Ping(ctx)
	return nil
}

// Close close the resource.
func (s *Service) Close() {
	//s.dao.Close()
	// 注销服务
	if cli, err := server.RegisterService(); err != nil {
		panic(err)
	} else if _, err := cli.Cancel(context.Background(), &pb.IdenSvcReqs{AppID: s.AppID()}); err != nil {
		panic(err)
	}
	// 停止hub和client
	// @_@: 好像这里有点问题
	for _, cli := range s.cliMapper {
		s.hub.Unregister <- cli
	}
}
