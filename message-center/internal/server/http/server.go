package http

import (
	"net/http"
	"message-center/internal/service"
	"message-center/internal/server/mws"

	"github.com/bilibili/kratos/pkg/conf/paladin"
	"github.com/bilibili/kratos/pkg/log"
	bm "github.com/bilibili/kratos/pkg/net/http/blademaster"
)

var (
	svc *service.Service
)

// New new a bm server.
func New(s *service.Service) (engine *bm.Engine) {
	var (
		hc struct {
			Server *bm.ServerConfig
		}
	)
	if err := paladin.Get("http.toml").UnmarshalTOML(&hc); err != nil {
		if err != paladin.ErrNotExist {
			panic(err)
		}
	}
	svc = s
	engine = bm.DefaultServer(hc.Server)
	engine.Use(mws.SetupCORS())
	// 开启一个HTTP接口用来升级处理WebSocket请求
	initRouter(engine, svc)
	if err := engine.Start(); err != nil {
		panic(err)
	}
	return
}

func initRouter(e *bm.Engine, s *service.Service) {
	e.Ping(ping)
	e.GET("/ws", func(ctx *bm.Context) {
		if err := s.ServeWs(ctx.Writer, ctx.Request); err != nil {
			ctx.AbortWithStatus(http.StatusInternalServerError)
		} else {
			ctx.JSON(&struct{
				Resp string
			}{
				Resp: "Connected !",
			}, nil)
		}
	})
}

func ping(ctx *bm.Context) {
	if err := svc.Ping(ctx); err != nil {
		log.Error("ping error(%v)", err)
		ctx.AbortWithStatus(http.StatusServiceUnavailable)
	}
}
