package grpc

import (
	pb "message-center/api"
	"message-center/internal/service"
	svr "message-center/internal/server"

	"github.com/bilibili/kratos/pkg/conf/paladin"
	"github.com/bilibili/kratos/pkg/net/rpc/warden"
)

// New new a grpc server.
func New(svc *service.Service) *warden.Server {
	var rc struct {
		Server *warden.ServerConfig
	}
	if err := paladin.Get("grpc.toml").UnmarshalTOML(&rc); err != nil {
		if err != paladin.ErrNotExist {
			panic(err)
		}
	}
	ws := warden.NewServer(rc.Server)
	pb.RegisterMessageCenterServer(ws.Server(), svc)
	svr.RegisterGRPCService(svc.AppID(), "SERVICE_ADDRS")
	ws, err := ws.Start()
	if err != nil {
		panic(err)
	}
	return ws
}
