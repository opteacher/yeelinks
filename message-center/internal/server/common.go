package server

import (
	"context"
	"fmt"
	"os"
	pb "message-center/api"
	"github.com/bilibili/kratos/pkg/net/rpc/warden"
	"strings"
)

func RegisterService() (pb.RegisterClient, error) {
	cli := warden.NewClient(nil)
	conn, err := cli.Dial(context.Background(), "discovery://default/register.service")
	if err != nil {
		return nil, fmt.Errorf("Register center is unready: %v\n", err)
	}
	return pb.NewRegisterClient(conn), nil
}

func RegisterGRPCService(appID string, addrsKey string) {
	addrs := os.Getenv(addrsKey)
	if len(addrs) == 0 {
		addrs = "127.0.0.1:9090"
	}
	if cli, err := RegisterService(); err != nil {
		panic(err)
	} else if resp, err := cli.RegAsGRPC(context.Background(), &pb.RegSvcReqs{
		AppID: appID,
		Urls:  strings.Split(addrs, ","),
	}); err != nil {
		panic(err)
	} else {
		fmt.Println(resp)
	}
}
