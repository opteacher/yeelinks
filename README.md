# yeelinks微服务后台框架
## 组成
|项目名|功能|端口|微服务名|
|-|-|-|-|
|`register-center` [注册中心]()|提供其他微服务的注册|9093|register.service|
|`message-center` [消息中心]()|接受传感器消息并推送至终端|9094|message.center.service|
## 部署
方式：通过docker部署项目
1. 下载项目`git clone git@github.com:opteacher/yeelinks.git`
2. 修改微服务的部署地址为公网可访问地址：修改`docker-compose.yml`中，`discovery`以下所有微服务项目的部署地址`SERVICE_ADDRS`改为部署主机的公网地址
3. 修改微服务启动时的注册中心地址：`-discovery.nodes 101.132.155.87:7171`改为部署本注册中心的公网地址
    > 101.132.155.87是一台阿里云虚拟主机，上面跑了一个注册中心。尝试用docker的内网容器名访问微服务均失败，所以在部署的时候，尽量都以公网可访问IP地址为准，不要使用容器名或者127.0.0.1
4. 在build过程中如需使用代理，修改`HTTP_PROXY`；如不需要，删除之
5. 修改对外端口`ports`，GRPC服务使用90XX端口；HTTP服务使用80XX端口（**注意不要跟其他微服务重合**）
6. 在根目录执行`docker-compose up -d --build`编译并执行所有微服务。全部编译完毕并正常运行之后可通过对外端口检验是否正常。HTTP服务可直接通过POSTMAN调用；GRPC服务则使用注册中心或者直连的方式调用
    > 注册中心：`conn, err := cli.Dial(context.Background(), "discovery://default/XXXX.service")`  
    直连：`conn, err := cli.Dial(context.Background(), "direct://default/X.X.X.X:90XX")`